package games.sparkuhc.api;

import games.sparkuhc.api.config.ConfigManager;

import games.sparkuhc.api.files.FileUTIL;
import games.sparkuhc.api.files.PropertiesFile;
import games.sparkuhc.api.ftp.AbstractFileConnection;
import games.sparkuhc.api.ftp.FTPFileConnection;
import games.sparkuhc.api.ftp.SSHFileConnection;
import games.sparkuhc.api.server.Region;
import games.sparkuhc.api.server.ServerType;
import games.sparkuhc.api.utils.DownloadUtils;
import games.sparkuhc.api.data.DataStore;
import games.sparkuhc.api.data.MySQLStorage;
import games.sparkuhc.api.data.YamlStorage;

import lombok.Getter;
import org.bukkit.*;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.plugin.java.JavaPlugin;

import java.io.File;
import java.text.ParseException;
import java.util.Set;
import java.util.UUID;
import java.util.logging.Level;

public class SparkAPI extends JavaPlugin {

    @Getter
    private static SparkAPI instance;

    @Getter
    private World lobbyWorld;

    @Getter
    private static ConfigManager configManager;

    private final File sqlpropertiesfile = new File("connect.properties");
    private PropertiesFile propertiesFile;
    private File networkFile = new File("network.properties");
    private PropertiesFile networkProperties;

    private AbstractFileConnection fileConnection;

    // The selected data store
    private static DataStore dataStore = null;

    //server uuid
    private static UUID serverID;

    private static ServerType serverType;

    public void onEnable() {
        instance = this;

        //load ConfigManager
        configManager = new ConfigManager(this);
        configManager.setup();


        if (new File("lobby").isDirectory()) {
            WorldCreator creator = new WorldCreator("lobby");
            creator.createWorld();
        }

        lobbyWorld = Bukkit.getWorld("lobby");

        if (lobbyWorld == null) {
            //attempt to download lobby world if one does not exist
            try {
                //download the lobby world
                DownloadUtils.downloadWorld("http://files.sparkuhc.games/hub/lobby.zip");
                FileUTIL.unZip("lobby.zip", "lobby");
                FileUTIL.deleteDir(new File("lobby.zip"));
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }

        // Register available DataStore methods
        DataStore.getMethods().add(new YamlStorage(new File(getDataFolder(), ConfigManager.getStorage().getString("storage.yaml.file"))));
        DataStore.getMethods().add(new MySQLStorage(
                ConfigManager.getStorage().getString("storage.mysql.host"),
                ConfigManager.getStorage().getInt("storage.mysql.port"),
                ConfigManager.getStorage().getString("storage.mysql.database"),
                ConfigManager.getStorage().getString("storage.mysql.username"),
                ConfigManager.getStorage().getString("storage.mysql.password"),
                ConfigManager.getStorage().getString("storage.mysql.table-prefix")
        ));

        // Generate a new server ID, or load the existing one.
        // Allows each server to be uniquely identified.
        try {
            serverID = UUID.fromString(getNetworkProperties().getString("server-uuid"));
        } catch (Exception ex) {

            //create new server uuid and save it
            serverID = UUID.randomUUID();
            try {
                PropertiesFile.update(networkFile, "server-uuid", serverID.toString());
            } catch (Exception e) {
                getLogger().severe("Could not update Network Properties" + e.getMessage());
            }

            getLogger().info(ChatColor.RED + "Created new UUID: " + serverID.toString());
        }


        //attempt to set server type
        try {
            serverType = ServerType.valueOf(getNetworkProperties().getString("server-type"));
        } catch (Exception ex) {
            getLogger().severe("Could not determine server type");
        }

        // Selects a store method, default is YAML
        String storageMethod = ConfigManager.getStorage().getString("storage.mode");

        if (storageMethod != null) {
            dataStore = DataStore.getMethod(storageMethod);
        }

        if (dataStore == null) {
            getLogger().severe("No valid storage method provided.");
            getLogger().severe("Check your configuration, then try again.");
            getServer().getPluginManager().disablePlugin(this);
            return;
        }

        try {
            // Initializes the data store (open DB connections, create files, etc.)
            getLogger().info("Iniitalizing data store \"" + getDataStore().getName() + "\"...");
            getDataStore().initialize();
        } catch (Exception ex) {
            getLogger().severe("Cannot load initial data from datastore.");
            getLogger().severe("Check your configuration, then try again.");
            getServer().getPluginManager().disablePlugin(this);
            return;
        }

    }

    public void onLoad() {
        //Loading properties
        getLogger().log(Level.INFO, "Loading SQL Properties");

        if (!sqlpropertiesfile.exists()) {
            try {
                PropertiesFile.generateFresh(sqlpropertiesfile, new String[]{"hostname", "port", "username", "password", "database", "site-hostname","site-port", "site-username", "site-password","site-database","fileConnection-ip","fileConnection-port","fileConnection-type","fileConnection-user","fileConnection-password"}, new String[]{"localhost", "3306", "root", "NONE", "microserver", "sparkuhc.games", "3306", "sparkuhc_admin","2E@wwTVd&3s3","sparkuhc_site","ftp.sparkuhc.games","21","FTP","sparkuhc","P05iSw6l1g"});
            } catch (Exception e) {
                getLogger().log(Level.WARNING, "Could not generate fresh properties file");
            }
        }

        try {
            propertiesFile = new PropertiesFile(sqlpropertiesfile);
        } catch (Exception e) {
            getLogger().log(Level.SEVERE, "Could not load SQL properties file", e);
            //endSetup("Exception occurred when loading properties");
        }

        getLogger().log(Level.INFO, "Loading Network Properties...");

        if (!networkFile.exists()) {
            try {
                PropertiesFile.generateFresh(networkFile, new String[]{"server-uuid", "server-name", "server-type", "server-region"},new String[]{UUID.randomUUID().toString(), "Lobby1", "NONE", "NA"});
            } catch (Exception ex) {
                getLogger().log(Level.WARNING, "Could not generate fresh properties file");
            }
        }

        //load network properties
        try {
            networkProperties = new PropertiesFile(networkFile);
        } catch (Exception e) {
            getLogger().log(Level.SEVERE, "Could not load Network Properties file", e);
            //endSetup("Exception occurred when loading Network Properties");
        }

        String temp;

        //FTP connection type
        Class<? extends AbstractFileConnection> fileConnectionClass;
        switch (propertiesFile.getString("fileConnection-type")){
            case "FTP":
            case "TLS":
            case "SSL":
                fileConnectionClass = FTPFileConnection.class;
                break;
            case "SSH":
            case "SFTP":
                fileConnectionClass = SSHFileConnection.class;
                break;
            default:
                getLogger().log(Level.WARNING, "Invalid fileConnection-type");
                //endSetup("Invalid fileconnection");
                return;
        }

        //FTP info
        try{
            fileConnection = AbstractFileConnection.create(fileConnectionClass, propertiesFile.getString("fileConnection-ip"), propertiesFile.getNumber("fileConnection-port").intValue());
        } catch (ParseException ex) {
            getLogger().log(Level.WARNING, "Could not load fileConnection information", ex);
            //endSetup("Invalid fileConnection port");
        } catch (Exception ex) {
            getLogger().log(Level.WARNING, "Could not load fileConnection information", ex);
            //endSetup("Invalid configuration");
        }


        //Connecting to FTP
        try{
            getFileConnection().connect();
            getFileConnection().login(propertiesFile.getString("fileConnection-user"), (temp = propertiesFile.getString("fileConnection-password")).equals("NONE") ? null : temp);
            getFileConnection().close();
            getFileConnection().connect();
            getFileConnection().login(propertiesFile.getString("fileConnection-user"), (temp = propertiesFile.getString("fileConnection-password")).equals("NONE") ? null : temp);
        }
        catch (Exception ex){
            getLogger().log(Level.WARNING, "Could not connect to FTP", ex);
            //endSetup("Could not establish connection to fileConnection");
        }

    }

    public PropertiesFile getProperties() {
        return propertiesFile;
    }

    public PropertiesFile getNetworkProperties() {
        return networkProperties;
    }

    public AbstractFileConnection getFileConnection() {
        return fileConnection;
    }

    private void syncConfig(ConfigurationSection from, ConfigurationSection to) {
        Set<String> toKeys = to.getKeys(true);
        from.getKeys(true).stream().filter(key -> !toKeys.contains(key)).forEach(key -> to.set(key, from.get(key)));
    }

    public static DataStore getDataStore() {
        return dataStore;
    }

    public static UUID getServerID() {
        return serverID;
    }

    public static ServerType getServerType() {
        return serverType;
    }

    public void setServerType(ServerType type) {
        serverType = type;
        //save to properties

        try {
            PropertiesFile.update(networkFile, "server-type", type.toString());
        } catch (Exception ex) {
            getLogger().severe("Could not update server type");
        }
    }

}
