package games.sparkuhc.api.config;

import games.sparkuhc.api.SparkAPI;
import games.sparkuhc.api.utils.FileUtils;
import lombok.Getter;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;

import java.io.*;

public class ConfigManager {

    private SparkAPI plugin;

    //config
    private File configFile;
    private File storageFile;


    @Getter
    private static FileConfiguration config;

    @Getter
    private static FileConfiguration storage;


    public ConfigManager(SparkAPI plugin) {
        this.plugin = plugin;
    }

    public void setup() {

        //make sure directory exists
        if (!plugin.getDataFolder().isDirectory()) {
            plugin.getDataFolder().mkdir();
        }

        //config files
        configFile = new File(plugin.getDataFolder(), "config.yml");
        storageFile = new File(plugin.getDataFolder(), "storage.yml");

        //setup the config manager
        if(!configFile.exists()) {
            //load the config file
            FileUtils.loadFile("config.yml");
        }

        if (!storageFile.exists()) {
            //load the storage file
            FileUtils.loadFile("storage.yml");
        }

        //attempt to load the file configurations
        reloadConfig();
        reloadStorage();

    }

    public void reloadConfig() {
        try {
            config = new YamlConfiguration();
            config.load(configFile);
        } catch (Exception ex) {
            plugin.getLogger().severe("Could not load config");
        }
    }

    public void reloadStorage() {
        try {
            storage = new YamlConfiguration();
            storage.load(storageFile);
        } catch (Exception ex){
            plugin.getLogger().severe("Could not load storage config");
        }
    }

    public void saveConfig() {
        try {
          config.save(configFile);
        } catch (IOException ex) {
            plugin.getLogger().severe("Could not save config");
        }
    }

    public void saveStorage() {
        try {
            storage.save(storageFile);
        } catch (IOException ex) {
            plugin.getLogger().severe("Could not save storage config");
        }
    }

}
