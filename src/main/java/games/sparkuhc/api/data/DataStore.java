package games.sparkuhc.api.data;

import games.sparkuhc.api.server.lobby.LobbyServer;
import games.sparkuhc.api.server.uhc.UHCServer;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

public abstract class DataStore {

    private static List<DataStore> methods = new ArrayList<>();

    public static DataStore getMethod(String name) {
        for (DataStore store : getMethods()) {
            if (store.getName().equalsIgnoreCase(name)) {
                return store;
            }
        }
        return null;
    }

    public static List<DataStore> getMethods() {
        return methods;
    }

    private String name;

    public DataStore(String name) {
        this.name = name;
    }

    public abstract void initialize();

    public abstract void close();

    //public abstract User loadUser(UUID uuid);

    //public abstract User loadUser(String name);

    //public abstract User getStats(UUID uuid);

    //public abstract void saveUser(User user);

    public abstract UHCServer loadUHCServer(UUID uuid);

    public abstract UHCServer loadUHCServer(String name);

    public abstract void loadUHCServers();

    public abstract void saveUHCServer(UHCServer server);

    public abstract LobbyServer loadLobbyServer(UUID uuid);

    public abstract LobbyServer loadLobbyServer(String name);

    public abstract void loadLobbyServers();

    public abstract void saveLobbyServer(LobbyServer server);

    public final String getName() {
        return name;
    }

}
