package games.sparkuhc.api.data;

import games.sparkuhc.api.SparkAPI;
import games.sparkuhc.api.config.ConfigManager;
import org.bukkit.scheduler.BukkitRunnable;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.logging.Level;

public final class MySQLStorage extends SQLDataStore {

    private String host;
    private int port;
    private String database;
    private String username;
    private String password;
    private String tablePrefix;

    public MySQLStorage(String host, int port, String database, String username, String password, String tablePrefix) {
        super("mysql");
        this.host = host;
        this.port = port;
        this.database = database;
        this.username = username;
        this.password = password;
        this.tablePrefix = tablePrefix;
    }

    @Override
    protected Connection openConnection() throws SQLException {
        return DriverManager.getConnection("jdbc:mysql://" + getHost() + ":" + getPort() + "/" + getDatabase(), getUsername(), getPassword());
    }

    @Override
    protected void setupTables() throws SQLException {
        try (PreparedStatement stmt = getConnection().prepareStatement("CREATE TABLE IF NOT EXISTS `uhc_servers` (" +
                "`id` BIGINT NOT NULL AUTO_INCREMENT, " +
                "`uuid` varchar(64) NOT NULL," +
                "`name` varchar(32) NOT NULL," +
                "`type` enum('FFA','TEAM') NOT NULL," +
                "`region` enum('NA','SA', 'EU','AU','ASIA') NOT NULL," +
                "`state` enum('LOADING','JOINABLE', 'LOBBY', 'INGAME', 'DEATHMATCH', 'ENDED','RESTARTING') NOT NULL," +
                "`scenario` varchar(100)," +
                "`count` int(11) NOT NULL," +
                "`max` int(11) NOT NULL," +
                "`teamsize` int(11)," +
                "PRIMARY KEY (`id`)," +
                "KEY `uuid_index` (`uuid`)" +
                ") ENGINE = InnoDB;")) {
            stmt.execute();
        }

        try (PreparedStatement stmt = getConnection().prepareStatement("CREATE TABLE IF NOT EXISTS `lobby_servers` (" +
                "`id` BIGINT NOT NULL AUTO_INCREMENT, " +
                "`uuid` varchar(64) NOT NULL," +
                "`name` varchar(32) NOT NULL," +
                "`region` enum('NA','SA', 'EU','AU','ASIA') NOT NULL," +
                "`state` enum('LOADING','JOINABLE', 'LOBBY', 'INGAME', 'DEATHMATCH', 'ENDED','RESTARTING') NOT NULL," +
                "`count` int(11) NOT NULL," +
                "`max` int(11) NOT NULL," +
                "PRIMARY KEY (`id`)," +
                "KEY `uuid_index` (`uuid`)" +
                ") ENGINE = InnoDB;")) {
            stmt.execute();
        }

        try (PreparedStatement stmt = getConnection().prepareStatement("CREATE TABLE IF NOT EXISTS `playerdata` (" +
                "`id` BIGINT NOT NULL AUTO_INCREMENT, " +
                "`uuid` varchar(64) NOT NULL," +
                "`name` varchar(32) NOT NULL," +
                "`rank` enum('OWNER','MANAGER','ADMIN','SRMOD','MOD','HELPER','YOUTUBE','TWITCH','IRON','GOLD','DIAMOND','EMERALD','MEMBER') NOT NULL," +
                "PRIMARY KEY (`id`)," +
                "KEY `uuid_index` (`uuid`)" +
                ") ENGINE = InnoDB;")) {
            stmt.execute();
        }

    }

    @Override
    public void initialize() {
        super.initialize();

        int mins = ConfigManager.getStorage().getInt("storage.mysql.sync-minutes", 0);
        if (mins > 0) {
            long secs = mins * 60;
            secs = secs * 20;
            new BukkitRunnable() {
                @Override
                public void run() {
                    SparkAPI.getInstance().getLogger().log(Level.INFO, "Resyncing data...");
                    loadUHCServers();
                    loadLobbyServers();
                }
            }.runTaskTimerAsynchronously(SparkAPI.getInstance(), secs, secs);
        }

    }

    public String getHost() {
        return host;
    }

    public int getPort() {
        return port;
    }

    public String getDatabase() {
        return database;
    }

    public String getUsername() {
        return username;
    }

    public String getPassword() {
        return password;
    }

    public String getTablePrefix() {
        return tablePrefix;
    }

}
