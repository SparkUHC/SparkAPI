package games.sparkuhc.api.data;

import games.sparkuhc.api.SparkAPI;
import games.sparkuhc.api.server.*;
import games.sparkuhc.api.server.lobby.LobbyServer;
import games.sparkuhc.api.server.uhc.UHCServer;
import games.sparkuhc.api.user.Rank;
import games.sparkuhc.api.user.User;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import java.util.logging.Level;

public abstract class SQLDataStore extends DataStore {

    private Connection connection;

    public SQLDataStore(String name) {
        super(name);
    }

    protected abstract Connection openConnection() throws SQLException;

    protected abstract void setupTables() throws SQLException;

    protected String getTablePrefix() {
        return "";
    }

    public final Connection getConnection() {
        return connection;
    }

    public boolean isConnected() {
        if(connection==null) {
            return false;
        }
        try {
            return !connection.isClosed();
        } catch (SQLException e) {
            return false;
        }
    }

    private void checkConnection() {
        if(!isConnected()) {
            throw new IllegalStateException("Database is not connected!");
        }
    }

    @Override
    public void initialize() {
        SparkAPI.getInstance().getLogger().log(Level.INFO, "Establishing " + this.getName() + " database connection...");
        try {
            connection = this.openConnection();

            SparkAPI.getInstance().getLogger().log(Level.INFO, "Connection successful! Checking tables...");
            this.setupTables();

            SparkAPI.getInstance().getLogger().log(Level.INFO, "SQL startup complete.");

        } catch (SQLException e) {
            close();
            throw new RuntimeException(e);
        }
    }

    @Override
    public void close() {
        try {
            if (getConnection() != null && !getConnection().isClosed()) {
                getConnection().close();
            }
        } catch (SQLException e) {
            throw new RuntimeException("Failed to properly close SQL connection", e);
        } finally {
            connection = null;
        }
    }

    @Override
    public UHCServer loadUHCServer(UUID uuid) {
        checkConnection();
        try (PreparedStatement stmt = getConnection().prepareStatement("SELECT * FROM uhc_servers WHERE uuid = ? LIMIT 1;")) {
            stmt.setString(1, uuid.toString());
            try(ResultSet set = stmt.executeQuery()) {
                return getUHCSQLServer(set);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return null;
    }

    @Override
    public UHCServer loadUHCServer(String name) {
        checkConnection();
        try (PreparedStatement stmt = getConnection().prepareStatement("SELECT * FROM uhc_servers WHERE server = ? LIMIT 1;")) {
            stmt.setString(1, name);
            try(ResultSet set = stmt.executeQuery()) {
                return getUHCSQLServer(set);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return null;
    }

    @Override
    public LobbyServer loadLobbyServer(UUID uuid) {
        checkConnection();
        try (PreparedStatement stmt = getConnection().prepareStatement("SELECT * FROM lobby_servers WHERE uuid = ? LIMIT 1;")) {
            stmt.setString(1, uuid.toString());
            try(ResultSet set = stmt.executeQuery()) {
                return getLobbySQLServer(set);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return null;
    }

    @Override
    public LobbyServer loadLobbyServer(String name) {
        checkConnection();
        try (PreparedStatement stmt = getConnection().prepareStatement("SELECT * FROM lobby_servers WHERE server = ? LIMIT 1;")) {
            stmt.setString(1, name);
            try(ResultSet set = stmt.executeQuery()) {
                return getLobbySQLServer(set);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return null;
    }

    private UHCServer getUHCSQLServer(ResultSet set) throws SQLException {
        try {
            if (set.next()) {

                String name = set.getString("name");
                UUID uuid = UUID.fromString(set.getString("uuid"));
                String scenario = set.getString("scenario");
                int count = set.getInt("count");
                int max = set.getInt("max");
                int teamSize = set.getInt("teamsize");
                ServerState state = ServerState.valueOf(set.getString("state"));
                Region region = Region.valueOf(set.getString("region"));
                GameType gameType = GameType.valueOf(set.getString("type"));

                UHCServer server = new UHCServer(uuid);

                //server settings
                server.setName(name);
                server.setState(state);
                server.setRegion(region);
                server.setCount(count);
                server.setMax(max);

                //game settings
                server.setScenario(scenario);
                server.setTeamSize(teamSize);
                server.setGameType(gameType);

                return server;

            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    private LobbyServer getLobbySQLServer(ResultSet set) throws SQLException {
        try {
            if (set.next()) {

                String name = set.getString("name");
                UUID uuid = UUID.fromString(set.getString("uuid"));
                int count = set.getInt("count");
                int max = set.getInt("max");
                ServerState state = ServerState.valueOf(set.getString("state"));
                Region region = Region.valueOf(set.getString("region"));

                LobbyServer server = new LobbyServer(uuid);

                //server settings
                server.setName(name);
                server.setState(state);
                server.setRegion(region);
                server.setCount(count);
                server.setMax(max);

                return server;

            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public void loadUHCServers() {
        checkConnection();

        try {
            List<UUID> downloadedServers = new ArrayList<>();
            PreparedStatement stmt = getConnection().prepareStatement("SELECT uuid FROM uhc_servers;");
            ResultSet set = stmt.executeQuery();
            while (set.next()) {
                UUID uuid = UUID.fromString(set.getString("uuid"));
                downloadedServers.add(uuid);
                if (ServerManager.getUHCServer(uuid) == null && SparkAPI.getServerType().equals(ServerType.UHC)) {
                    ServerManager.getUHCServers().add(new UHCServer(SparkAPI.getServerID()));
                }
            }
            set.close();
            stmt.close();
            if (downloadedServers.size() > 0) {
                for (int i = 0; i < ServerManager.getUHCServers().size(); i++) {
                    UHCServer server = ServerManager.getUHCServers().get(i);
                    if (!downloadedServers.contains(server.getUUUID())) {
                        ServerManager.getUHCServers().remove(server); // server was deleted
                        //group.delete();
                    }
                }
            }
            for (UHCServer server : ServerManager.getUHCServers()) {
                stmt = getConnection().prepareStatement("SELECT * FROM uhc_servers WHERE uuid = ? LIMIT 1;");
                stmt.setString(1, server.getUUUID().toString());
                set = stmt.executeQuery();
                if (set.next()) {

                    //server settings
                    server.setName(set.getString("name"));
                    server.setState(ServerState.valueOf(set.getString("state")));
                    server.setRegion(Region.valueOf(set.getString("region")));
                    server.setCount(set.getInt("count"));
                    server.setMax(set.getInt("max"));

                    //game settings
                    server.setScenario(set.getString("scenario"));
                    server.setTeamSize(set.getInt("teamsize"));
                    server.setGameType(GameType.valueOf(set.getString("type")));

                }
                set.close();
                stmt.close();
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

    }

    @Override
    public void loadLobbyServers() {
        checkConnection();

        try {
            List<UUID> downloadedServers = new ArrayList<>();
            PreparedStatement stmt = getConnection().prepareStatement("SELECT uuid FROM lobby_servers;");
            ResultSet set = stmt.executeQuery();
            while (set.next()) {
                UUID uuid = UUID.fromString(set.getString("uuid"));
                downloadedServers.add(uuid);
                if (ServerManager.getLobbyServer(uuid) == null && SparkAPI.getServerType().equals(ServerType.LOBBY)) {
                    ServerManager.getLobbyServers().add(new LobbyServer(SparkAPI.getServerID()));
                }
            }
            set.close();
            stmt.close();
            if (downloadedServers.size() > 0) {
                for (int i = 0; i < ServerManager.getUHCServers().size(); i++) {
                    LobbyServer server = ServerManager.getLobbyServers().get(i);
                    if (!downloadedServers.contains(server.getUUUID())) {
                        ServerManager.getLobbyServers().remove(server); // server was deleted
                        //group.delete();
                    }
                }
            }
            for (LobbyServer server : ServerManager.getLobbyServers()) {
                stmt = getConnection().prepareStatement("SELECT * FROM lobby_servers WHERE uuid = ? LIMIT 1;");
                stmt.setString(1, server.getUUUID().toString());
                set = stmt.executeQuery();
                if (set.next()) {

                    //server settings
                    server.setName(set.getString("name"));
                    server.setState(ServerState.valueOf(set.getString("state")));
                    server.setRegion(Region.valueOf(set.getString("region")));
                    server.setCount(set.getInt("count"));
                    server.setMax(set.getInt("max"));

                }
                set.close();
                stmt.close();
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

    }

    @Override
    public void saveUHCServer(UHCServer server) {
        checkConnection();

        String uuid = server.getUUUID().toString();

        try {
            PreparedStatement stmt = getConnection().prepareStatement("SELECT * FROM uhc_servers WHERE uuid = ? LIMIT 1;");
            stmt.setString(1, server.getUUUID().toString());
            ResultSet set = stmt.executeQuery();
            if (set.next()) {
                stmt.close();

                //update name
                stmt = getConnection().prepareStatement("UPDATE uhc_servers SET name = ? WHERE uuid = ?;");
                stmt.setString(1, server.getName());
                stmt.setString(2, uuid);
                stmt.execute();
                stmt.close();

                //update region
                stmt = getConnection().prepareStatement("UPDATE uhc_servers SET region = ? WHERE uuid = ?;");
                stmt.setString(1, server.getRegion().toString());
                stmt.setString(2, uuid);
                stmt.execute();
                stmt.close();

                //update game type
                stmt = getConnection().prepareStatement("UPDATE uhc_servers SET type = ? WHERE uuid = ?;");
                stmt.setString(1, server.getGameType().toString());
                stmt.setString(2, uuid);
                stmt.execute();
                stmt.close();

                //update state
                stmt = getConnection().prepareStatement("UPDATE uhc_servers SET state = ? WHERE uuid = ?;");
                stmt.setString(1, server.getState().toString());
                stmt.setString(2, uuid);
                stmt.execute();
                stmt.close();

                //update scenario
                stmt = getConnection().prepareStatement("UPDATE uhc_servers SET scenario = ? WHERE uuid = ?;");
                stmt.setString(1, server.getScenario());
                stmt.setString(2, uuid);
                stmt.execute();
                stmt.close();

                //update count
                stmt = getConnection().prepareStatement("UPDATE uhc_servers SET count = ? WHERE uuid = ?;");
                stmt.setInt(1, server.getCount());
                stmt.setString(2, uuid);
                stmt.execute();
                stmt.close();

                //update max
                stmt = getConnection().prepareStatement("UPDATE uhc_servers SET max = ? WHERE uuid = ?;");
                stmt.setInt(1, server.getMax());
                stmt.setString(2, uuid);
                stmt.execute();
                stmt.close();

                //update team size
                stmt = getConnection().prepareStatement("UPDATE uhc_servers SET teamsize = ? WHERE uuid = ?");
                stmt.setInt(1, server.getTeamSize());
                stmt.setString(2, uuid);
                stmt.execute();
                stmt.close();

            } else {

                stmt.close();
                stmt = getConnection().prepareStatement("INSERT INTO uhc_servers (uuid, name, type, state, region, scenario, count, max, teamsize) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?);");
                stmt.setString(1, uuid);
                stmt.setString(2, server.getName());
                stmt.setString(3, server.getGameType() == null ? GameType.FFA.toString() : server.getGameType().toString());
                stmt.setString(4, server.getState() == null ? ServerState.LOADING.toString() : server.getState().toString());
                stmt.setString(5, server.getRegion() == null ? Region.NA.toString() : server.getRegion().toString());
                stmt.setString(6, server.getScenario().isEmpty() ? "None" : server.getScenario());
                stmt.setInt(7, server.getCount());
                stmt.setInt(8, server.getMax());
                stmt.setInt(9, server.getTeamSize());
                stmt.execute();
                stmt.close();

            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

    }

    @Override
    public void saveLobbyServer(LobbyServer server) {
        checkConnection();

        String uuid = server.getUUUID().toString();

        try {
            PreparedStatement stmt = getConnection().prepareStatement("SELECT * FROM lobby_servers WHERE uuid = ? LIMIT 1;");
            stmt.setString(1, server.getUUUID().toString());
            ResultSet set = stmt.executeQuery();
            if (set.next()) {
                stmt.close();

                //update name
                stmt = getConnection().prepareStatement("UPDATE lobby_servers SET name = ? WHERE uuid = ?;");
                stmt.setString(1, server.getName());
                stmt.setString(2, uuid);
                stmt.execute();
                stmt.close();

                //update region
                stmt = getConnection().prepareStatement("UPDATE lobby_servers SET region = ? WHERE uuid = ?;");
                stmt.setString(1, server.getRegion().toString());
                stmt.setString(2, uuid);
                stmt.execute();
                stmt.close();

                //update state
                stmt = getConnection().prepareStatement("UPDATE lobby_servers SET state = ? WHERE uuid = ?;");
                stmt.setString(1, server.getState().toString());
                stmt.setString(2, uuid);
                stmt.execute();
                stmt.close();

                //update count
                stmt = getConnection().prepareStatement("UPDATE lobby_servers SET count = ? WHERE uuid = ?;");
                stmt.setInt(1, server.getCount());
                stmt.setString(2, uuid);
                stmt.execute();
                stmt.close();

                //update max
                stmt = getConnection().prepareStatement("UPDATE lobby_servers SET max = ? WHERE uuid = ?;");
                stmt.setInt(1, server.getMax());
                stmt.setString(2, uuid);
                stmt.execute();
                stmt.close();


            } else {

                stmt.close();
                stmt = getConnection().prepareStatement("INSERT INTO lobby_servers (uuid, name, state, region, count, max) VALUES (?, ?, ?, ?, ?, ?);");
                stmt.setString(1, uuid);
                stmt.setString(2, server.getName());
                stmt.setString(3, server.getState() == null ? ServerState.LOADING.toString() : server.getState().toString());
                stmt.setString(4, server.getRegion() == null ? Region.NA.toString() : server.getRegion().toString());
                stmt.setInt(5, server.getCount());
                stmt.setInt(6, server.getMax());
                stmt.execute();
                stmt.close();

            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

    }

    public void saveUser(User user) {
        checkConnection();

        String uuid = user.getUUID().toString();

        try {
            PreparedStatement stmt = getConnection().prepareStatement("SELECT * FROM playerdata WHERE uuid = ? LIMIT 1;");
            stmt.setString(1, user.getUUID().toString());
            ResultSet set = stmt.executeQuery();
            if (set.next()) {
                stmt.close();

                //update name
                stmt = getConnection().prepareStatement("UPDATE playerdata SET name = ? WHERE uuid = ?;");
                stmt.setString(1, user.getName());
                stmt.setString(2, uuid);
                stmt.execute();
                stmt.close();

                //update rank
                stmt = getConnection().prepareStatement("UPDATE playerdata SET region = ? WHERE uuid = ?;");
                stmt.setString(1, user.getRank().toString());
                stmt.setString(2, uuid);
                stmt.execute();
                stmt.close();


            } else {

                stmt.close();
                stmt = getConnection().prepareStatement("INSERT INTO playerdata (uuid, name, rank) VALUES (?, ?, ?);");
                stmt.setString(1, uuid);
                stmt.setString(2, user.getName());
                stmt.setString(3, user.getRank() == null ? Rank.MEMBER.toString() : user.getRank().toString());
                stmt.execute();
                stmt.close();

            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

    }



}
