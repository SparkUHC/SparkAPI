package games.sparkuhc.api.data;

import games.sparkuhc.api.SparkAPI;
import games.sparkuhc.api.server.*;
import games.sparkuhc.api.SparkAPI;
import games.sparkuhc.api.server.lobby.LobbyServer;
import games.sparkuhc.api.server.uhc.UHCServer;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.configuration.InvalidConfigurationException;
import org.bukkit.configuration.file.YamlConfiguration;

import java.io.File;
import java.io.IOException;
import java.util.Set;
import java.util.UUID;
import java.util.logging.Level;

// Allows saving config and users to a yml file in the plugin's directory
public final class YamlStorage extends DataStore {

    private YamlConfiguration config;
    private File file;

    public YamlStorage(File file) {
        super("yaml");
        this.file = file;
    }

    public YamlConfiguration getConfig() {
        return config;
    }

    public File getFile() {
        return file;
    }

    // Creates and loads necessary storage files
    @Override
    public void initialize() {
        config = new YamlConfiguration();

        if (!getFile().exists()) {
            try {
                if (getFile().createNewFile()) {
                    SparkAPI.getInstance().getLogger().log(Level.INFO, "Created storage file: " + getFile().getName());
                }
            } catch (IOException e) {
                throw new RuntimeException(e);
            }
        }

        try {
            getConfig().load(getFile());
        } catch (IOException | InvalidConfigurationException e) {
            e.printStackTrace();
            throw new RuntimeException(e);
        }
    }

    // Writes changes to disk
    public void save() {
        try {
            getConfig().save(getFile());
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public void close() {
        save();
    }

    // Loads server data from configuration
    private UHCServer loadUHCServerData(UUID uuid) {
        try {
            String path = "uhc_servers." + uuid.toString();

            String name = getConfig().getString(path + ".name");
            String scenario = getConfig().getString(path + ".scenario");
            int count = getConfig().getInt(path + ".count");
            int max = getConfig().getInt(path + ".max");
            int teamSize = getConfig().getInt(path + ".team-size");
            ServerState state = ServerState.valueOf(getConfig().getString(path + ".state"));
            GameType gameType = GameType.valueOf(getConfig().getString(path + ".type"));
            Region region = Region.valueOf(getConfig().getString(path + ".region"));

            if (name == null) {
                return null;
            }

            UHCServer server = new UHCServer(uuid);
            server.setName(name);
            server.setState(state);
            server.setGameType(gameType);
            server.setRegion(region);
            server.setScenario(scenario);
            server.setCount(count);
            server.setMax(max);
            server.setTeamSize(teamSize);

            return server;
        } catch (Exception ex) {
            return null;
        }
    }

    private LobbyServer loadLobbyServerData(UUID uuid) {
        try {
            String path = "lobby_servers." + uuid.toString();

            String name = getConfig().getString(path + ".name");
            int count = getConfig().getInt(path + ".count");
            int max = getConfig().getInt(path + ".max");
            ServerState state = ServerState.valueOf(getConfig().getString(path + ".state"));
            Region region = Region.valueOf(getConfig().getString(path + ".region"));

            if (name == null) {
                return null;
            }

            LobbyServer server = new LobbyServer(uuid);
            server.setName(name);
            server.setState(state);
            server.setRegion(region);
            server.setCount(count);
            server.setMax(max);

            return server;
        } catch (Exception ex) {
            return null;
        }
    }

    // Passes UUID to private method
    @Override
    public UHCServer loadUHCServer(UUID uuid) {
        return loadUHCServerData(uuid);
    }

    // SLOW - Go by UUID if possible, resolves username to UUID, if no match, returns null
    @Override
    public UHCServer loadUHCServer(String name) {

        UUID uuid = null;
        ConfigurationSection section = getConfig().getConfigurationSection("uhc_servers");
        if (section != null) {
            Set<String> uuids = section.getKeys(false);
            if (uuids != null) {
                for (String id : uuids) {
                    String path = "uhc_servers." + id;
                    String n = getConfig().getString(path + ".name");
                    if (n.equalsIgnoreCase(name)) {
                        uuid = UUID.fromString(id);
                        break;
                    }
                }
            }

        }

        return uuid != null ? loadUHCServerData(uuid) : null;
    }

    // Passes UUID to private method
    @Override
    public LobbyServer loadLobbyServer(UUID uuid) {
        return loadLobbyServerData(uuid);
    }

    // SLOW - Go by UUID if possible, resolves username to UUID, if no match, returns null
    @Override
    public LobbyServer loadLobbyServer(String name) {

        UUID uuid = null;
        ConfigurationSection section = getConfig().getConfigurationSection("lobby_servers");
        if (section != null) {
            Set<String> uuids = section.getKeys(false);
            if (uuids != null) {
                for (String id : uuids) {
                    String path = "lobby_servers." + id;
                    String n = getConfig().getString(path + ".name");
                    if (n.equalsIgnoreCase(name)) {
                        uuid = UUID.fromString(id);
                        break;
                    }
                }
            }

        }

        return uuid != null ? loadLobbyServerData(uuid) : null;
    }

    // Writes user data to file
    @Override
    public void saveUHCServer(UHCServer server) {

        String path = "uhc_servers." + server.getUUUID().toString();

        //server settings
        getConfig().set(path + ".name", server.getName());
        getConfig().set(path + ".state", server.getState().toString());
        getConfig().set(path + ".type", server.getGameType().toString());
        getConfig().set(path + ".region", server.getRegion().toString());
        getConfig().set(path + ".scenario", server.getScenario());
        getConfig().set(path + ".count", server.getCount());
        getConfig().set(path + ".max", server.getMax());
        getConfig().set(path + ".team-size", server.getTeamSize());

        save();
    }

    // Writes user data to file
    @Override
    public void saveLobbyServer(LobbyServer server) {

        String path = "lobby_servers." + server.getUUUID().toString();

        //server settings
        getConfig().set(path + ".name", server.getName());
        getConfig().set(path + ".state", server.getState().toString());
        getConfig().set(path + ".region", server.getRegion().toString());
        getConfig().set(path + ".count", server.getCount());
        getConfig().set(path + ".max", server.getMax());

        save();
    }

    @Override
    public void loadUHCServers() {


        ConfigurationSection section = getConfig().getConfigurationSection("uhc_servers");
        if (section != null) {
            Set<String> uuids = section.getKeys(false);
            if (uuids != null) {

                for (String id : uuids) {
                    ServerManager.getUHCServers().add(new UHCServer(UUID.fromString(id)));
                }

                for (String id : uuids) {
                    UHCServer server = ServerManager.getUHCServer(UUID.fromString(id));

                    if (server == null) {
                        return;
                    }

                    String path = "uhc_servers." + id;

                    //server settings
                    String name = getConfig().getString(path + ".name");
                    String scenario = getConfig().getString(path + ".scenario");
                    int count = getConfig().getInt(path + ".count");
                    int max = getConfig().getInt(path + ".max");
                    int teamSize = getConfig().getInt(path + ".team-size");
                    ServerState state = ServerState.valueOf(getConfig().getString(path + ".state"));
                    GameType gameType = GameType.valueOf(getConfig().getString(path + ".type"));
                    Region region = Region.valueOf(getConfig().getString(path + ".region"));

                    server.setName(name);
                    server.setState(state);
                    server.setGameType(gameType);
                    server.setRegion(region);
                    server.setScenario(scenario);
                    server.setCount(count);
                    server.setMax(max);
                    server.setTeamSize(teamSize);
                }

            }
        }


    }

    @Override
    public void loadLobbyServers() {


        ConfigurationSection section = getConfig().getConfigurationSection("lobby_servers");
        if (section != null) {
            Set<String> uuids = section.getKeys(false);
            if (uuids != null) {

                for (String id : uuids) {
                    ServerManager.getLobbyServers().add(new LobbyServer(UUID.fromString(id)));
                }

                for (String id : uuids) {
                    LobbyServer server = ServerManager.getLobbyServer(UUID.fromString(id));

                    if (server == null) {
                        return;
                    }

                    String path = "lobby_servers." + id;

                    //server settings
                    String name = getConfig().getString(path + ".name");
                    int count = getConfig().getInt(path + ".count");
                    int max = getConfig().getInt(path + ".max");
                    ServerState state = ServerState.valueOf(getConfig().getString(path + ".state"));
                    Region region = Region.valueOf(getConfig().getString(path + ".region"));

                    server.setName(name);
                    server.setState(state);
                    server.setRegion(region);
                    server.setCount(count);
                    server.setMax(max);
                }

            }
        }


    }

}
