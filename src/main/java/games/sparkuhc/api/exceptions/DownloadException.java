package games.sparkuhc.api.exceptions;

public class DownloadException extends Exception {

    public DownloadException(final String message) {
        super(message);
    }
}
