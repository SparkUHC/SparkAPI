package games.sparkuhc.api.server;

public enum Region {

    NA,
    SA,
    EU,
    AU,
    ASIA

}
