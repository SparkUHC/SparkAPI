package games.sparkuhc.api.server;

import games.sparkuhc.api.SparkAPI;
import games.sparkuhc.api.server.lobby.LobbyServer;
import games.sparkuhc.api.server.uhc.UHCServer;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
public class ServerManager {

    // A list of all server currently connected
    private static List<LobbyServer> lobbyServers = new ArrayList<>();
    private static List<UHCServer> uhcServers = new ArrayList<>();

    // Matches a server to UUID
    // If none is found, searches DataStore
    public static UHCServer getUHCServer(UUID uuid) {
        for (UHCServer server : getUHCServers()) {
            if (server.getUUUID().equals(uuid)) {
                return server;
            }
        }
        return SparkAPI.getDataStore().loadUHCServer(uuid);
    }

    // Matches a server by name
    // If none is found, searches DataStore
    public static UHCServer getUHCServer(String name) {
        for (UHCServer server : getUHCServers()) {
            if (server.getName() != null && server.getName().equalsIgnoreCase(name)) {
                return server;
            }
        }

        return SparkAPI.getDataStore().loadUHCServer(name);
    }

    public static LobbyServer getLobbyServer(UUID uuid) {
        for (LobbyServer server : getLobbyServers()) {
            if (server.getUUUID().equals(uuid)) {
                return server;
            }
        }

        return SparkAPI.getDataStore().loadLobbyServer(uuid);
    }

    public static LobbyServer getLobbyServer(String name) {
        for (LobbyServer server : getLobbyServers()) {
            if (server.getName() != null && server.getName().equalsIgnoreCase(name)) {
                return server;
            }
        }

        return SparkAPI.getDataStore().loadLobbyServer(name);
    }

    public static List<UHCServer> getUHCServers() {
        return uhcServers;
    }

    public static List<LobbyServer> getLobbyServers() {
        return lobbyServers;
    }
}
