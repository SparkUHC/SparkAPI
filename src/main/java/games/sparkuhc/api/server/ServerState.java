package games.sparkuhc.api.server;

public enum ServerState {

    LOADING,
    JOINABLE,
    LOBBY,
    INGAME,
    DEATHMATCH,
    ENDED,
    RESTARTING

}
