package games.sparkuhc.api.server.lobby;

import games.sparkuhc.api.server.GameType;
import games.sparkuhc.api.server.Region;
import games.sparkuhc.api.server.ServerState;

import java.util.UUID;

public class LobbyServer {

    //server settings
    private UUID uuid;
    private String name;
    private long lastHeartbeat = 0;
    private ServerState state;
    private Region region;
    private int count;
    private int max;

    public LobbyServer(UUID uuid) {
        this.uuid = uuid;
    }

    //getters
    public UUID getUUUID() {
        return uuid;
    }

    public String getName() {
        return name;
    }

    public long getLastHeartbeat() {
        return lastHeartbeat;
    }

    public ServerState getState() {
        return state;
    }

    public Region getRegion() {
        return region;
    }

    public int getCount() {
        return count;
    }

    public int getMax() {
        return max;
    }

    //setters
    public void setName(String name) {
        this.name = name;
    }

    public void setLastHeartbeat(long lastHeartbeat) {
        this.lastHeartbeat = lastHeartbeat;
    }

    public void setState(ServerState state) {
        this.state = state;
    }

    public void setRegion(Region region) {
        this.region = region;
    }

    public void setCount(int count) {
        this.count = count;
    }

    public void setMax(int max) {
        this.max = max;
    }

}
