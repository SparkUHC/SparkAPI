package games.sparkuhc.api.user;

public enum Rank {

    OWNER,
    MANAGER,
    ADMIN,
    SrMOD,
    MOD,
    HELPER,
    TWITCH,
    YOUTUBE,
    IRON,
    GOLD,
    DIAMOND,
    EMERALD,
    MEMBER

}
