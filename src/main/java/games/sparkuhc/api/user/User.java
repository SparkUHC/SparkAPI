package games.sparkuhc.api.user;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;

import java.util.UUID;

public class User {

    //common player data
    private final UUID uuid;
    private String name = null;
    private Player player = null;

    //player permissions data
    private Rank rank;

    public User(UUID uuid) {
        this.uuid = uuid;
    }

    //getters
    public UUID getUUID() {
        return uuid;
    }

    public Player getPlayer() {
        return player;
    }

    public String getName() {
        return name;
    }

    public Rank getRank() {
        return rank;
    }

    public String getMessagePrefix(){

        return getPexPrefix() + getName() + getPexSuffix();
    }

    public String getPexPrefix(){
        return user.getPrefix() == null ? "" : ChatColor.translateAlternateColorCodes('&', permissionUser.getPrefix());
    }

    public String getPexSuffix(){
        return permissionUser.getSuffix() == null ? "" : ChatColor.translateAlternateColorCodes('&', permissionUser.getSuffix());
    }


    //setters
    public void setName(String name) {
        this.name = name;
    }

    public void setRank(Rank rank) {
        //sets the users rank
        this.rank = rank;
    }

    public boolean isOnline(){
        return player != null && player.isOnline();
    }

    public boolean isStaff(){
        return rank == Rank.OWNER || rank == Rank.ADMIN || rank == Rank.SrMOD || rank == Rank.MOD || rank == Rank.HELPER;
    }


}
