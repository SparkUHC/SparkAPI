package games.sparkuhc.api.user;

import com.sk89q.worldedit.entity.Player;
import games.sparkuhc.api.SparkAPI;

import java.util.Collection;
import java.util.UUID;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

public class UserManager {

    private SparkAPI plugin;
    private ConcurrentMap<UUID, User> userMap = new ConcurrentHashMap<>();

    public UserManager(SparkAPI plugin) {
        this.plugin = plugin;
    }

    public ConcurrentMap<UUID, User> getUserMap() {
        return userMap;
    }

    public Collection<User> getUsers(){
        return userMap.values();
    }

    public User getUser(Player player) {
        return  player == null ? null : userMap.get(player.getUniqueId());
    }

    public User procesJoin(Player p){
        User user = getUserMap().get(p.getUniqueId());

        if(user == null){
            user = new User(p.getUniqueId());
            getUserMap().put(p.getUniqueId(), user);
        }

        return user;
    }



}
