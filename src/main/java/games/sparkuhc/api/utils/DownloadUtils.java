package games.sparkuhc.api.utils;

import games.sparkuhc.api.SparkAPI;

import java.io.*;
import java.net.HttpURLConnection;

public class DownloadUtils {

    public static void downloadFile(String fileURL, String saveDir) throws IOException {

        java.net.URL url = new java.net.URL(fileURL);
        HttpURLConnection httpConn = (HttpURLConnection)url.openConnection();
        httpConn.addRequestProperty("User-Agent", "Mozilla/4.0");
        int responseCode = httpConn.getResponseCode();


        if (responseCode == 200) {
            String fileName = "";
            String disposition = httpConn.getHeaderField("Content-Disposition");
            String contentType = httpConn.getContentType();
            int contentLength = httpConn.getContentLength();

            if (disposition != null) {

                int index = disposition.indexOf("filename=");
                if (index > 0) {
                    fileName = disposition.substring(index + 10, disposition.length() - 1);
                }

            } else {

                fileName = fileURL.substring(fileURL.lastIndexOf("/") + 1, fileURL.length());

            }


            System.out.println("Content-Type = " + contentType);
            System.out.println("Content-Disposition = " + disposition);
            System.out.println("Content-Length = " + contentLength);
            System.out.println("fileName = " + fileName);


            InputStream inputStream = httpConn.getInputStream();
            String saveFilePath = saveDir + File.separator + fileName;


            FileOutputStream outputStream = new FileOutputStream(saveFilePath);

            int bytesRead = -1;
            byte[] buffer = new byte['?'];
            while ((bytesRead = inputStream.read(buffer)) != -1) {
                outputStream.write(buffer, 0, bytesRead);
            }

            outputStream.close();
            inputStream.close();


        } else {
            System.out.println("No file to download. Server replied HTTP code: " + responseCode + ", URL: " + fileURL);
        }

        httpConn.disconnect();
    }

    public static void downloadPluginFile(String fileURL) throws IOException {

        java.net.URL url = new java.net.URL(fileURL);
        HttpURLConnection httpConn = (HttpURLConnection)url.openConnection();
        httpConn.addRequestProperty("User-Agent", "Mozilla/4.0");
        int responseCode = httpConn.getResponseCode();


        if (responseCode == 200) {
            String fileName = "";
            String disposition = httpConn.getHeaderField("Content-Disposition");
            String contentType = httpConn.getContentType();
            int contentLength = httpConn.getContentLength();

            if (disposition != null) {

                int index = disposition.indexOf("filename=");
                if (index > 0) {
                    fileName = disposition.substring(index + 10, disposition.length() - 1);
                }

            } else {

                fileName = fileURL.substring(fileURL.lastIndexOf("/") + 1, fileURL.length());

            }


            System.out.println("Content-Type = " + contentType);
            System.out.println("Content-Disposition = " + disposition);
            System.out.println("Content-Length = " + contentLength);
            System.out.println("fileName = " + fileName);


            InputStream inputStream = httpConn.getInputStream();
            String saveFilePath = SparkAPI.getInstance().getDataFolder() + File.separator + fileName;


            FileOutputStream outputStream = new FileOutputStream(saveFilePath);

            int bytesRead = -1;
            byte[] buffer = new byte['?'];
            while ((bytesRead = inputStream.read(buffer)) != -1) {
                outputStream.write(buffer, 0, bytesRead);
            }

            outputStream.close();
            inputStream.close();


        } else {
            System.out.println("No file to download. Server replied HTTP code: " + responseCode + ", URL: " + fileURL);
        }

        httpConn.disconnect();
    }

    public static void downloadWorld(String fileURL) throws IOException {

        java.net.URL url = new java.net.URL(fileURL);
        HttpURLConnection httpConn = (HttpURLConnection)url.openConnection();
        httpConn.addRequestProperty("User-Agent", "Mozilla/4.0");
        int responseCode = httpConn.getResponseCode();


        if (responseCode == 200) {
            String fileName = "";
            String disposition = httpConn.getHeaderField("Content-Disposition");
            String contentType = httpConn.getContentType();
            int contentLength = httpConn.getContentLength();

            if (disposition != null) {

                int index = disposition.indexOf("filename=");
                if (index > 0) {
                    fileName = disposition.substring(index + 10, disposition.length() - 1);
                }

            } else {

                fileName = fileURL.substring(fileURL.lastIndexOf("/") + 1, fileURL.length());

            }


            System.out.println("Content-Type = " + contentType);
            System.out.println("Content-Disposition = " + disposition);
            System.out.println("Content-Length = " + contentLength);
            System.out.println("fileName = " + fileName);


            InputStream inputStream = httpConn.getInputStream();
            String saveFilePath = fileName;


            FileOutputStream outputStream = new FileOutputStream(saveFilePath);

            int bytesRead = -1;
            byte[] buffer = new byte['?'];
            while ((bytesRead = inputStream.read(buffer)) != -1) {
                outputStream.write(buffer, 0, bytesRead);
            }

            outputStream.close();
            inputStream.close();


        } else {
            System.out.println("No file to download. Server replied HTTP code: " + responseCode + ", URL: " + fileURL);
        }

        httpConn.disconnect();
    }

}
