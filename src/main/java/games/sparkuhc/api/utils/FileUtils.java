package games.sparkuhc.api.utils;


import games.sparkuhc.api.SparkAPI;

import java.io.*;
import java.util.logging.Level;

public class FileUtils {

    public static void loadFile(String file)
    {
        File t = new File(SparkAPI.getInstance().getDataFolder(), file);
        SparkAPI.getInstance().getLogger().log(Level.INFO, "Writing new file: " + t.getAbsolutePath());

        try {
            t.createNewFile();
            FileWriter out = new FileWriter(t);
            //InputStream is = getClass().getResourceAsStream("/"+file);
            InputStream is = SparkAPI.getInstance().getResource(file);
            InputStreamReader isr = new InputStreamReader(is);
            BufferedReader br = new BufferedReader(isr);
            String line;
            while ((line = br.readLine()) != null) {
                out.write(line + "\n");
            }
            out.flush();
            is.close();
            isr.close();
            br.close();
            out.close();

            SparkAPI.getInstance().getLogger().log(Level.INFO, "Loaded Config: " + file + "successfully!");

        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    public static boolean moveFile(File oldConfig) {
        SparkAPI.getInstance().getLogger().log(Level.INFO, "Moving outdated config file: " + oldConfig.getName());
        String name = oldConfig.getName();
        File configBackup = new File(SparkAPI.getInstance().getDataFolder(), getNextName(name, 0));
        return oldConfig.renameTo(configBackup);
    }

    private static String getNextName(String name, int n){
        File oldConfig = new File(SparkAPI.getInstance().getDataFolder(), name+".old"+n);
        if(!oldConfig.exists()){
            return oldConfig.getName();
        }
        else{
            return getNextName(name, n+1);
        }
    }

}
